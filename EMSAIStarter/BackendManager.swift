//
//  BackendManager.swift
//  EMSAIStarter
//
//  Created by Vijay Raj on 2017-11-24.
//  Copyright © 2017 TELUS. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

private let _singleton = BackendManager()

class BackendManager {
    fileprivate let HACK_URL = "https://hack2017.mbenablers.com/bikes"
    fileprivate let OCR = "/ocr"                // Google vision API
    fileprivate let TRANSCRIPT = "/transcript"  // Google's and IBM's speech to text
    fileprivate let SENTIMENT = "/sentiment"    // Sentiment analysis via Google NLP
    fileprivate let ALPR = "/alpr"              // Automatic License Plate Recognition
    fileprivate let TWEET = "/twitter"          // Tweet analysis
    // Static access token for the purpose of the AI hackathon
    fileprivate let ACCESS_TOKEN = "9gMbELAXLHLTE5mghQxHw9KqsNeEvsQmzTNmzFE7DcdELCNEuYuUpyMp4AnhcVr2"
    
    // Singleton instance
    class var single: BackendManager {
        return _singleton
    }
    
    // Sentiment Analysis
    func performSentimentAnalysis(with text:String, callback: @escaping (String) -> Void) {
        let parameters:[String:Any] = [
            "token" : ACCESS_TOKEN,
            "document": [
                "content" : text
            ]
        ]
        
        Alamofire.request(HACK_URL + SENTIMENT, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { (response) in
                if let jsonvalue = response.result.value {
                    print("JSON \(jsonvalue)")
                    
                    let json = JSON(jsonvalue)
                    
                    if json["error"] != JSON.null {
                        // error
                        callback("ERROR, " + json["error"].debugDescription)
                        return
                    }
                    
                    var retStr = json["message"].debugDescription + "\n"
                    
                    if json["data"]["documentSentiment"] != JSON.null {
                        // Got data
                        let magnitude = json["data"]["documentSentiment"]["magnitude"].debugDescription
                        let score = json["data"]["documentSentiment"]["score"].debugDescription
                        retStr = retStr + "magnitude = " + magnitude + ". score = " + score
                        callback(retStr)
                    } else {
                        callback(retStr)
                    }
                } else {
                    callback("ERROR, NO RESULT VALUE")
                }
        }
    }
    
    // OCR
    func performCharacterRocog(with image:UIImage, callback: @escaping (String) -> Void) {
        let base64Str = UtilityHelper.single.convertImageToBase64(with: image)
        let parameters:[String:Any] = [
            "token" : ACCESS_TOKEN,
            "image" : base64Str
        ]
        
        Alamofire.request(HACK_URL + OCR, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { (response) in
                if let jsonvalue = response.result.value {
                    print("JSON \(jsonvalue)")
                    
                    let json = JSON(jsonvalue)
                    
                    if json["error"] != JSON.null {
                        // error
                        callback("ERROR, " + json["error"].debugDescription)
                        return
                    }
                    
                    var retStr = json["message"].debugDescription + "\n"
                    if json["data"][0] != JSON.null {
                        // Got data
                        let description = json["data"][0]["description"].debugDescription
                        retStr = retStr + "description = " + description
                        callback(retStr)
                    } else {
                        callback(retStr)
                    }
                } else {
                    callback("ERROR, NO RESULT VALUE")
                }
        }
    }
    
    // Automatic License Plate Recognition
    func performALPR(with image:UIImage, callback: @escaping (String) -> Void) {
        let base64Str = UtilityHelper.single.convertImageToBase64(with: image)
        let parameters:[String:Any] = [
            "token" : ACCESS_TOKEN,
            "image" : base64Str
        ]
        
        Alamofire.request(HACK_URL + ALPR, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { (response) in
                if let jsonvalue = response.result.value {
                    print("JSON \(jsonvalue)")

                    let json = JSON(jsonvalue)

                    if json["error"] != JSON.null {
                        // error
                        callback("ERROR, " + json["error"].debugDescription)
                        return
                    }

                    var retStr = json["message"].debugDescription + "\n"
                    if json["data"]["results"] != JSON.null {
                        // Got data
                        let results = json["data"]["results"].debugDescription
                        retStr = retStr + "results = " + results
                        callback(retStr)
                    } else {
                        callback(retStr)
                    }
                } else {
                    callback("ERROR, NO RESULT VALUE")
                }
        }
    }
    
    // Tweet Analysis
    func performTweetAnalysis(for twitter_user:String, callback: @escaping (String) -> Void) {
        let parameters:[String:Any] = [
            "token" : ACCESS_TOKEN,
            "path"  : "statuses/user_timeline",
            "params"  : [
                "screen_name" : twitter_user
            ]
        ]
        
        /*
         {
         "token": "MQKMJXvURxbZZ",
         "path": "statuses/user_timeline",
         "params": {
         "screen_name": "robmclarty"
         }
         }
        */
        
        Alamofire.request(HACK_URL + TWEET, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { (response) in
                if let jsonvalue = response.result.value {
                    print("JSON \(jsonvalue)")
                    
                    let json = JSON(jsonvalue)
                    
                    if json["error"] != JSON.null {
                        // error
                        callback("ERROR, " + json["error"].debugDescription)
                        return
                    }
                    
                    var retStr = json["message"].debugDescription + "\n"
                    
                    if json["data"]["documentSentiment"] != JSON.null {
                        // Got data
                        let magnitude = json["data"]["documentSentiment"]["magnitude"].debugDescription
                        let score = json["data"]["documentSentiment"]["score"].debugDescription
                        retStr = retStr + "magnitude = " + magnitude + ". score = " + score
                        callback(retStr)
                    } else {
                        callback(retStr)
                    }
                } else {
                    callback("ERROR, NO RESULT VALUE")
                }
        }
    }
    
    // Speech To Text
    func performSpeechToText(with audioFilePathURL:URL, callback: @escaping (String) -> Void) {
        let base64Str = UtilityHelper.single.convertAudioToBase64(with: audioFilePathURL)
        let parameters:[String:Any] = [
            "token" : ACCESS_TOKEN,
            "audio" : base64Str,
            "config": [
                "encoding" : "LINEAR16",
                "sampleRateHertz" : 32000,
                "languageCode" : "en-GB"
            ]
        ]
        
        print("parameters: " + parameters.debugDescription)
        
        Alamofire.request(HACK_URL + TRANSCRIPT, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { (response) in
                if let jsonvalue = response.result.value {
                    print("JSON \(jsonvalue)")
                    
                    let json = JSON(jsonvalue)
                    
                    if json["error"] != JSON.null {
                        // error
                        callback("ERROR, " + json["error"].debugDescription)
                        return
                    }
                    
                    var retStr = json["message"].debugDescription + "\n"
                    if json["data"] != JSON.null {
                        // Got data
                        let description = json["data"].debugDescription
                        retStr = retStr + "description = " + description
                        callback(retStr)
                    } else {
                        callback(retStr)
                    }
                } else {
                    callback("ERROR, NO RESULT VALUE")
                }
        }
    }
    
    // Authenticate
    /**
    func authenticate(username: String, password: String, callback: @escaping (Bool) -> Void) {
        let parameters = ["username": username, "password": password]
        
        Alamofire.request(AUTH_URL + AUTH_TOKEN, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { (response) in
                
                if let jsonvalue = response.result.value {
                    print("JSON \(jsonvalue)")
                    
                    // Parse accessToken and put it
                    let json = JSON(jsonvalue)
                    
                    if json["error"] != JSON.null {
                        // error
                        callback(true)
                        return
                    }
                    
                    let access_token = json["tokens"]["accessToken"].string
                    print("Access Token: \(access_token!)")
                    
                    let refresh_token = json["tokens"]["refreshToken"].string
                    print("Refresh Token: \(refresh_token!)")
                    
                    let isActive = AppManager.single.setAuthToken(with: access_token!, refresh: refresh_token!)
                    
                    /*
                     BackendManager.single.refresh(callback: { (error) in
                     
                     
                     if error == true {
                     print("Error \(error)")
                     return
                     }
                     print("Execute callback function")
                     })
                     */
                    
                    callback(!isActive)
                    return
                }
                
                callback(true)
        }
    }
 **/

}
