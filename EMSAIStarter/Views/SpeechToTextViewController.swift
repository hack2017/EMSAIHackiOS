//
//  SpeechToTextViewController.swift
//  EMSAIStarter
//
//  Created by Vijay Raj on 2017-11-27.
//  Copyright © 2017 TELUS. All rights reserved.
//

import UIKit
import AVFoundation

class SpeechToTextViewController: UIViewController {

    @IBOutlet weak var logView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playAudio(_ sender: UIButton) {
        let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "Cycle001", ofType: "wav")!)
        print(alertSound)
        
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try! AVAudioSession.sharedInstance().setActive(true)
        
        let audioPlayer = try! AVAudioPlayer(contentsOf: alertSound)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    @IBAction func analyze(_ sender: UIButton) {
        let audioURL = URL(fileURLWithPath: Bundle.main.path(forResource: "Cycle001", ofType: "wav")!)
        BackendManager.single.performSpeechToText(with: audioURL) { (response) in
            self.addToLog(with: response)
        }
    }
    
    @IBAction func closeView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addToLog(with log:String) -> Void {
        let currentLog:String = self.logView.text
        let newLog = log + "\n" + currentLog
        self.logView.text = newLog
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
