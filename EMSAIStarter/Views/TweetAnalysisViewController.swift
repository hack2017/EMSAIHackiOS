//
//  TweetAnalysisViewController.swift
//  EMSAIStarter
//
//  Created by Vijay Raj on 2017-11-27.
//  Copyright © 2017 TELUS. All rights reserved.
//

import UIKit

class TweetAnalysisViewController: UIViewController {

    @IBOutlet weak var logView: UITextView!
    @IBOutlet weak var tweetTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func analyze(_ sender: UIButton) {
        BackendManager.single.performTweetAnalysis(for: self.tweetTextField.text!) { (response) in
            self.addToLog(with: response)
        }
    }
    
    func addToLog(with log:String) -> Void {
        let currentLog:String = self.logView.text
        let newLog = log + "\n" + currentLog
        self.logView.text = newLog
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
