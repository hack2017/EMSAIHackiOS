//
//  UtilityHelper.swift
//  EMSAIStarter
//
//  Created by Vijay Raj on 2017-11-24.
//  Copyright © 2017 TELUS. All rights reserved.
//

import Foundation
import UIKit

private let _singleton = UtilityHelper()

class UtilityHelper {
    // Singleton instance
    class var single: UtilityHelper {
        return _singleton
    }
    
    // Convert image to base64 string
    func convertImageToBase64(with image:UIImage) -> String {
        //Use image name from bundle to create NSData
        // let image : UIImage = UIImage(named:imageName)!
        //Now use image to create into NSData format
        let imageData:Data = UIImagePNGRepresentation(image)!
    
        let strBase64 = imageData.base64EncodedString(options: .init(rawValue: 0))
        return strBase64
    }
    
    // Convert audio (mp3) to base64 string
    func convertAudioToBase64(with audioFilePathURL:URL) -> String {
        let fileData = try! Data.init(contentsOf: audioFilePathURL)
        let strBase64 = fileData.base64EncodedString(options: .init(rawValue: 0))
        return strBase64
    }
}
