# iOS Basic sample app

This app is a starting point for an user wanting to use the hackathon backend on iOS. A few components have been implemented for a developer to pick up and extend, or use as reference.

## Pods

This project uses cocoapods and some common pods. The developer does not need to intall cocoapods to use the project or extend it. Developer will need to open *BoilerPlate.xcworkspace* file in XCode instead of the project file to have the pods loaded and to build. These are the pods used:

- Alamofire - Networking helper framework
- SwiftyJSON - JSON parser framework

## Structure

This project is structured in a flat way with minimal complexity.

### Views

There are five views in the project each tying back to the backend API available for this hackathon. [https://hack2017.mbenablers.com/bikes/docs/index.html](https://hack2017.mbenablers.com/bikes/docs/index.html). Each of the views follow similar format with Close button, a log view to let the user know what is happening, and the item required for the respective API. E.g. Image view of the image that is sent for OCR analysis. Each of the view has the `analyze` method or function that starts the action to hit the backend.

- SentimentAnalysisViewController.swift
- APLRViewController.swift
- CharacterRecogViewController.swift
- SpeechToTextViewController.swift
- TweetAnalysisViewController.swift

### BackendManager.swift

This is the file that contains all interactions with our hackathon backend, it also contains constants with values of the backend url and API paths. 

- performSentimentAnalysis
- performCharacterRocog
- performALPR
- performTweetAnalysis
- performSpeechToText

### UtilityHelper.swift

A simple utility helper to convert image to base64 and audio to base64 strings. Image and audio is expected as base64 string by the backend for faster prototyping - for the purproses of this hackathon.

- convertImageToBase64
- convertAudioToBase64

### Data files

These are the data files hardcoded to the project. The purpose is to show how backend APIs can be levearged and how it can be implemented from iOS. The participants are expected to substitute this with their source and use it as buliding blocks.

- Cycle001.wav - audio file of feedback about bike lanes. This is recorded by a british actor, please udpate `config` in `BackendManager` based on who is speaking.
- coop_cab.png - An image of taxi with name plate visible.
- car_plate.png - An image of a car with name plate visible.
